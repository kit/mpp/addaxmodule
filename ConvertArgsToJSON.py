import argparse
import json
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-OutputDataPath', required=True, type=str)
    parser.add_argument('-ReceiverRange', required=True,type=float,nargs=2)
    parser.add_argument('-ReceiverSampling', required=True,type=int)
    parser.add_argument('-ResamplingFactor', required=True,type=int)
    parser.add_argument('-SeismogramDataFileName', required=True,type=str)
    parser.add_argument('-SeismogramDataPath', required=True,type=str)
    parser.add_argument('-SourceRange', required=True,type=float,nargs=2)
    parser.add_argument('-SourceSampling', required=True,type=int)
    parser.add_argument('-TimeRange', required=True,type=float,nargs=2)
    args = parser.parse_args()
    source_range = args.SourceRange
    source_sampling_factor = args.SourceSampling
    receiver_range = args.ReceiverRange
    receiver_sampling_factor = args.ReceiverSampling
    time_range = args.TimeRange
    data_resampling_factor = args.ResamplingFactor
    experiment_name = "{srcrange}__{srcsample}__{recrange}__{recsample}__{endtime}__dt_{dt}__".format(
        srcrange='src_{}-{}'.format(source_range[0], source_range[1]),
        srcsample='srcsamp_{}'.format(source_sampling_factor),
        recrange='rec_{}-{}'.format(receiver_range[0], receiver_range[1]),
        recsample='recsamp_{}'.format(receiver_sampling_factor), endtime='endtime_{}'.format(time_range[1]),
        dt=0.0009 * data_resampling_factor)
    f = open('SeismogramConfig.json', 'w')
    f.write(json.dumps({'OutputDataPath': args.OutputDataPath,
                        'ReceiverRange':args.ReceiverRange,
                        'ReceiverSampling': args.ReceiverSampling,
                        'ResamplingFactor': args.ResamplingFactor,
                        'SeismogramDataFileName': args.SeismogramDataFileName,
             'SourceRange':args.SourceRange, 'SourceSampling': args.SourceSampling,
             'TimeRange':args.TimeRange,'SeismogramDataPath': args.SeismogramDataPath,
                        }))