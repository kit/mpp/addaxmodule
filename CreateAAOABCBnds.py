import argparse

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-InnerBottomBnd', required=False, type=float, default=0.0)
    parser.add_argument('-InnerTopBnd', required=False, type=float, default=2200.0)
    parser.add_argument('-InnerLeftBnd', required=True, type=float)
    parser.add_argument('-InnerRightBnd', required=True, type=float)
    #parser.add_argument('-InnerLeftBnd', required=False, type=float,default=400.0)
    #parser.add_argument('-InnerRightBnd', required=False, type=float, default=9000)

    parser.add_argument('-DampingLayerBottom', required=False, type=float, default=0.0)
    parser.add_argument('-DampingLayerTop', required=False, type=float, default=500.0)

    parser.add_argument('-DampingLayerLeft', required=True, type=float)
    parser.add_argument('-DampingLayerRight', required=True, type=float)
    #parser.add_argument('-DampingLayerLeft', required=False, type=float,default=400.0)
    #parser.add_argument('-DampingLayerRight', required=False, type=float,default=400.0)

    parser.add_argument('-dx', required=False, type=float, default=8.5)
    args = parser.parse_args()
    InnerBottomBnd = args.InnerBottomBnd
    InnerTopBnd = args.InnerTopBnd
    InnerLeftBnd = args.InnerLeftBnd
    InnerRightBnd = args.InnerRightBnd
    DampingLayerBottom = args.DampingLayerBottom
    DampingLayerTop = args.DampingLayerTop
    DampingLayerLeft = args.DampingLayerLeft
    DampingLayerRight = args.DampingLayerRight
    dx = args.dx
    InnerBottomBnd = (InnerBottomBnd // dx)*dx
    InnerLeftBnd = (InnerLeftBnd // dx)*dx
    DampingLayerBottom = (DampingLayerBottom // dx) * dx if DampingLayerBottom > 1e-10 else 0
    DampingLayerTop = (DampingLayerTop // dx) * dx if DampingLayerTop > 1e-10 else 0
    DampingLayerLeft = (DampingLayerLeft // dx) * dx if DampingLayerLeft > 1e-10 else 0
    DampingLayerRight = (DampingLayerRight // dx) * dx if DampingLayerRight > 1e-10 else 0
    divisorX = 64
    divisorY = 64
    dimXInner = (InnerRightBnd - InnerLeftBnd) // dx
    dimYInner = (InnerTopBnd - InnerBottomBnd) // dx
    InnerTopBnd = dimYInner * dx + InnerBottomBnd
    InnerRightBnd = dimXInner * dx + InnerLeftBnd
    InnerXSize = InnerRightBnd - InnerLeftBnd
    InnerYSize = InnerTopBnd - InnerBottomBnd
    dimX = (InnerXSize + DampingLayerRight + DampingLayerLeft) // dx
    dimY = (InnerYSize + DampingLayerTop + DampingLayerBottom) // dx
    dimX = round(dimX / divisorX) * divisorX + 1
    dimY = round(dimY / divisorY) * divisorY + 1

    if dimX * dx < InnerXSize + DampingLayerRight + DampingLayerLeft:
        dimX = dimX + divisorX
    if dimY * dx < InnerYSize + DampingLayerTop + DampingLayerBottom:
        dimY = dimY + divisorY
    LeftBnd = InnerLeftBnd - DampingLayerLeft
    RightBnd = LeftBnd + (dimX-1)*dx
    BottomBnd = InnerBottomBnd - DampingLayerBottom
    TopBnd = BottomBnd + (dimY-1)*dx
    print(
        'InnerBottomBnd={}\n InnerTopBnd={} \nInnerLeftBnd={} \nInnerRightBnd={} \nBottomBnd={} \nTopBnd={} \nLeftBnd={} \nRightBnd={} \ndimX={} \ndimY={}'.format(
            InnerBottomBnd, InnerTopBnd, InnerLeftBnd, InnerRightBnd, BottomBnd, TopBnd, LeftBnd, RightBnd, dimX, dimY))
