import base64
import json
import os
import sys
import numpy as np


class Seismogram:
    def __init__(self, path, filename):

        data_file = ""
        try:
            json_data = json.load(open(path + filename))
            data_file = json_data["Data"]
            self.json_data = json_data['Observation Specification']
        except FileNotFoundError:
            print('Seismogram file not found')
        self.Traces = []
        fulldata = np.load(path + data_file)['arr_0']
        for i, rp in enumerate(self.json_data['Full Receiver List']):
            self.Traces.append(Trace(rp, fulldata[i, :], self.json_data))

    def slice(self, stop):
        for trace in self.Traces:
            trace.slice(stop)

    def resample(self, factor):
        for trace in self.Traces:
            trace.resample(factor)


class Trace:
    def __init__(self, pos, data, stats):
        self.sampling_rate = stats['SamplingRate']
        self.delta = stats['Delta']
        self.data = data
        self.npts = len(data)
        self.T = (self.npts-1) * self.delta
        self.pos = pos

    def resample(self, factor):
        self.data = self.data[::factor]
        self.delta = self.delta * factor
        self.sampling_rate = self.sampling_rate / factor
        self.npts = len(self.data)
        self.T = (self.npts-1) * self.delta

    def slice(self, end_time):
        if self.T < end_time:
            exit("Chosen slicetime is higher than the end of the trace")
        else:
            steps = int(end_time / self.delta)+1
            self.data = self.data[:steps:]
            self.T = (steps-1) * self.delta
            self.npts = len(self.data)


Config = json.loads(open("SeismogramConfig.json", 'r').read())


def catch_except(string_name):
    try:
        return Config[string_name]
    except KeyError:
        print(string_name + " not found in your Config file.")
        exit(1)
    else:
        pass
    finally:
        pass


seismogram_data_path = catch_except('SeismogramDataPath')
seismogram_data_file_name = catch_except('SeismogramDataFileName')

source_range = catch_except('SourceRange')
receiver_range = catch_except('ReceiverRange')
data_resampling_factor = catch_except('ResamplingFactor')
source_sampling_factor = catch_except('SourceSampling')
receiver_sampling_factor = catch_except('ReceiverSampling')
time_range = catch_except('TimeRange')
output_data_path = catch_except('OutputDataPath')

try:
    os.mkdir(output_data_path)
except FileExistsError:
    pass

receiver_positions = json.loads(open("Data/ReceiverPositions.json", 'r').read())['ReceiverPositions']
source_positions = json.loads(open("Data/SourcePositions.json", 'r').read())['SourcePositions']

receiver_positions = [x for x in receiver_positions if receiver_range[0] < x[0] < receiver_range[1]]
receiver_positions = receiver_positions[::receiver_sampling_factor]
source_index = [x for x in enumerate(source_positions) if source_range[0] < x[1][0] < source_range[1]]
source_index = source_index[::source_sampling_factor]
source_positions = [x[1] for x in source_index]
experiment_name = "{srcrange}__{srcsample}__{recrange}__{recsample}__{endtime}__dt_{dt}__".format(
    srcrange='src_{}-{}'.format(source_range[0], source_range[1]),
    srcsample='srcsamp_{}'.format(source_sampling_factor),
    recrange='rec_{}-{}'.format(receiver_range[0], receiver_range[1]),
    recsample='recsamp_{}'.format(receiver_sampling_factor), endtime='endtime_{}'.format(time_range[1]),
    dt=0.0009 * data_resampling_factor)
times = None
datanames =[]
relativepath = "addaxmodule/"
for (j,(shot, sp)) in enumerate(source_index):
    seismo = Seismogram(seismogram_data_path, seismogram_data_file_name.format(shot))
    seismo.slice(time_range[1])
    seismo.resample(data_resampling_factor)
    times = [0.0, seismo.Traces[0].T, seismo.Traces[0].delta]
    obs_spec = {"Full Receiver List": receiver_positions, "Times": times, "TimeSteps": int((times[1] - times[0]) / times[2]) + 1,
                     "Measure Components": 1}
    full = {"Observation Specification": obs_spec, "Data": {}}
    datanames.append(experiment_name + "__" + str(j) )
    f = open(output_data_path + datanames[-1] + '.json', 'w')

    for i, trace in enumerate(seismo.Traces):
        full['Data'][str(i)] = base64.b64encode(np.ascontiguousarray(trace.data)).decode('ascii') + "="
    f.write(json.dumps(full, ensure_ascii=False))
    f.close()

experiment_dict = {"Observation Specification": {"Full Receiver List": receiver_positions,"Times": times, "TimeSteps": int((times[1] - times[0]) / times[2]) + 1, "Measure Components": 1}, "Full Source List": source_positions,
                    "OutputDataPath":relativepath  + output_data_path,
                   "SourceRange": source_range, "ReceiverRange": receiver_range, "DataNames":  datanames}
g = open(output_data_path + experiment_name[:-2] + '.json', 'w')
g.write(json.dumps(experiment_dict, ensure_ascii=False))
print("ADDAXExperimentFile=addaxmodule/" + output_data_path + experiment_name[:-2] +'\n' + "Specfile=addaxmodule/"+ output_data_path + experiment_name[:-2])